from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import time

PATH = "C:\Program Files (x86)\chromedriver.exe"
driver = webdriver.Chrome(PATH)

driver.get("https://www.takeaway.com/be/")
try:
    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.ID, "imysearchstring"))
    )
    element.clear()

    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.ID, "submit_deliveryarea"))
    )
    element.click()

    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.CLASS_NAME, "restaurantname"))
    )
    element.click()
    element = driver.find_element_by_id("irestaurantN357RNQ")
    element.click()


except:
    print(driver.title)
