from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import time

PATH = "C:\Program Files (x86)\chromedriver.exe"
driver = webdriver.Chrome(PATH)

driver.get("https://www.zalando.be/heren-home/")
# driver.save_screenshot("screen.png")
try:
    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.ID, "uc-btn-accept-banner"))
    )
    element.click()
    element = driver.find_element_by_name("q")
    element.click()
    element.send_keys("Raph Lauren")
    element.send_keys(Keys.RETURN)
    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.LINK_TEXT, "Solden"))
    )
    element.click()
    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.CLASS_NAME, "_8Nfi4s"))
    )
    element.click()
    element = WebDriverWait(driver, 10).until(
        ec.presence_of_element_located((By.CLASS_NAME, "cat_count-1RzWf"))
    )
    text_file = open("aantalproducten.txt", "w")
    n = text_file.write(element.text)
    text_file.close()

except:
    print(driver.title)
